__author__ = "Chen-Hao Chen"
__copyright__ = "Copyright 2015, Chen-Hao Chen, Liu lab"
__email__ = "hyalin1127@gmail.com"
__license__ = "MIT"

import os
import sys
import time
import csv
from collections import defaultdict, Counter
from urllib.request import urlopen
import bz2
import gzip
import io
import logging


# TODO rewrite this with Pandas


class Annotator():
    def __init__(self, library, annotation_table):
        self.customized_table = annotation_table
        self.sequence_table = library
        self.sequence_dict = defaultdict(list)
        self.sequence_set = set()
        self.seq_match_record = defaultdict(list)
        self.non_gene_match_record = defaultdict(list)

    def annotate(self):
        self.sequence_table_import()
        self.custom_bed_get()
        self.write_output()

    def sequence_table_import(self):
        with open(self.sequence_table) as csvfile:
            reader = csv.reader(csvfile)
            for elements in reader:
                self.sequence_dict[elements[1].upper()] = [elements[0],
                                                           elements[2].upper()]
                self.sequence_set.add(elements[1].upper())

    def custom_bed_get(self):
        if self.customized_table.startswith("http"):
            file = urlopen(self.customized_table)
        else:
            file = open(self.customized_table, "rb")
        if self.customized_table.endswith(".bz2"):
            file = io.BufferedReader(bz2.open(file))
        elif self.customized_table.endswith(".gz"):
            file = io.BufferedReader(gzip.open(file))

        for i, line in enumerate(file):
            chr, chrstart, chrend, gene, score, strand, seq = line.decode(
            ).strip().split("\t")
            gene = gene.upper()
            seq = seq.upper()
            try:
                int(chrstart)
                int(chrend)
                float(score)

            except ValueError:
                raise SyntaxError(
                    "Error parsing line {} in annotation table.".format(i))

            if (seq in self.sequence_set):
                insert = [chr, chrstart, chrend, self.sequence_dict[seq][0],
                          score, strand]
                self.seq_match_record[seq].append(
                    insert + [gene, self.sequence_dict[seq][1], seq])

    def write_output(self):
        for values in self.seq_match_record.values():
            record = 0
            for i in values:
                if i[6] == i[7]:
                    print("{0}".format("\t".join(i[:6])))
                    record = 1

            if record == 0:
                print("{0}".format("\t".join(i[:6])))
                logging.warning("{0}".format("\t".join(
                    ["Warning: gene not matched"] + [i[k] for k in [3, 8, 7]] +
                    ["|"] + [i[k] for k in [0, 1, 2, 6]])))

        for j in [i for i in self.sequence_dict.keys()
                  if i not in self.seq_match_record.keys()]:
            temp = self.sequence_dict[j]
            logging.warning("{0}".format("\t".join(["Warning: sequence not found", temp[0],
                                          j, temp[1]])))
